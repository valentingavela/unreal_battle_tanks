// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "BattleTanks/Public/Tank.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANKS_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATank* GetControlledTank() const;

	virtual void BeginPlay() override;  //Si el metodo se declara virtual, 
								//puede ser sobreescrito por cualquier ancestro en el futuro
	
	virtual void Tick( float DeltaTime );

	void AimTowardsCrosshair();

	bool GetSightRayHitLocation(FVector &HitLocation) const;

	UPROPERTY(EditAnywhere)
	float CrosshairXLocation = 0.5;

	UPROPERTY(EditAnywhere)
	float CrosshairXLocation = 0.3333;
};
