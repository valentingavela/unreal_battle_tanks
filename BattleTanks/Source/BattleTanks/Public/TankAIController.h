// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Public/Tank.h"
#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANKS_API ATankAIController : public AAIController
{
	GENERATED_BODY()

public:
	ATank * GetControlledTank() const;

	virtual void BeginPlay() override;  //Si el metodo se declara virtual, 
										//puede ser sobreescrito por cualquier ancestro en el futuro
	
	ATank* GetPlayerTank() const;
	
	
};
